import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class RoyalRumble {

	/**
	 * @param name
	 * @return Parse royal name
	 */
	private String parseRoyalName(String name) {
		return name.split(" ")[0];
	}

	/**
	 * @param name
	 * @return Parse royal number.
	 */
	private int parseRoyalNumber(String name) {
		// split roman number from name.
		String number = name.split(" ")[1];

		// default map with respective char in roman number to real number.
		Map<Character, Integer> romanMap = new HashMap<>();
		romanMap.put('I', 1);
		romanMap.put('V', 5);
		romanMap.put('X', 10);
		romanMap.put('L', 50);

		// total of all roman characters.
		int total = 0;
		// to hold last real number.
		int temp = 0;

		/**
		 * Following is the algo to convert roman number to real number
		 * 
		 * 1. Init ${total } and ${temp} with 0; 
		 * 2. Start with last character string. 
		 * 3. Fetch real number from roman character. 
		 * 4. Check number from 3rd step with ${temp}. if number is less than or eqal to temp then add it to ${total} else
		 * subtract from ${total}. 
		 * 5. Set ${temp} with 3rd step value. 
		 * 6. Repeat 2..5 till first character of the roman number string.
		 */
		for (int i = (number.length() - 1); i >= 0; i--) {

			int rNumber = romanMap.get(number.charAt(i));

			if (temp <= rNumber) {
				total += rNumber;
			} else {
				total -= rNumber;
			}
			temp = rNumber;

		}

		return total;
	}

	public List<String> getSortedList(List<String> names) {
		return names.stream().map(royalName -> {
			String name = parseRoyalName(royalName);
			int number = parseRoyalNumber(royalName);

			return new RoyalName(name, number, royalName);
		}).sorted(Comparator.comparing(RoyalName::getName).thenComparingInt(RoyalName::getNumber))
				.map(RoyalName::getRoyalName).collect(Collectors.toList());		
	}
}
