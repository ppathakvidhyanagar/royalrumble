#!/bin/bash

find -name "*.java" > sources.txt
javac @sources.txt

for file in ./input*.txt
do
  java -cp . Main $file
  printf "\n"
done