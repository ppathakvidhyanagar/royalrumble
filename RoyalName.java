
/**
 * @author Pranav Pathak
 *
 */
public class RoyalName {
	
	private String name;
	
	private int number;
	
	private String royalName;	
	

	public RoyalName(String name, int number, String royalName) {
		super();
		this.name = name;
		this.number = number;
		this.royalName = royalName;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the number
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(int number) {
		this.number = number;
	}

	/**
	 * @return the royalName
	 */
	public String getRoyalName() {
		return royalName;
	}

	/**
	 * @param royalName the royalName to set
	 */
	public void setRoyalName(String royalName) {
		this.royalName = royalName;
	}
	
	
	
	
}
